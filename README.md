# Accounting Notebook (Rest API)

[API](https://polar-scrubland-52638.herokuapp.com)  developed to manage account notes related to a single user.
Below you will find the actions available for the user. 

## Resolution
* Database logic was encapsulated to make extensible to others implementations.
* Schema validation with Ajv middleware in controllers.
* Services responsable of bussines logic.
    
### API endpoints
URL : https://polar-scrubland-52638.herokuapp.com

* Get balance
```http
GET /api/transaction/balance
```
Response
```javascript
{
    "balance" : 100
}
```

* Get transaction list
```http
GET /api/transaction
```
Response
```javascript
[{
    "id" : "01811007-e64b-437f-93b3-a04fca7f40e8",
    "type" : "credit",
    "amount" : 100,
    "effectiveDate" : "2020-11-03T11:54:18.968Z"
},...,{}]
```
 
 * Get transaction details
```http
GET /api/transaction/{transactionId}
```
Response
```javascript
{
    "id" : "01811007-e64b-437f-93b3-a04fca7f40e8",
    "type" : "credit",
    "amount" : 100,
    "effectiveDate" : "2020-11-03T11:54:18.968Z"
}
```

* Commit a new transaction
```http
POST /api/transaction/
```
Request body
```javascript
{
  "type": "debit",
  "amount": 10
}
```
| Parameter | Type | Description |
| :--- | :--- | :--- |
| `type` | `string` | **Required**. Possible values : ["credit", "debit"]
| `amount` | `number` | **Required**. Positive value.
Response
```javascript
    "transaction stored"
``` 
 
