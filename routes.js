const router = require("express").Router();
const transaction = require("./controllers/transactions");

router.use("/transaction", transaction);

module.exports = router;
